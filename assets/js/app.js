import axios from "axios";
import{ User } from './user';
import { renderUser } from "./user";
import { createIcons } from "./icons";

const USERS = [];

const addNewUser = document.querySelector(".add-user");
const addNewUserBox = document.querySelector(".add-user-button");
const usersContainer = document.querySelector(".users_container");

console.log(addNewUser);

addNewUser.addEventListener('click', displayNewUser);
addNewUserBox.addEventListener('click', displayNewUser);


async function displayNewUser(){
    axios.get("https://randomuser.me/api/")
    .then((response) => {
        let currentData = response.data.results[0];

        let firstName = currentData.name.first
        let lastName = currentData.name.last
        let age = currentData.dob.age
        let email = currentData.email
        let streetNumber = currentData.location.street.number
        let streetName = currentData.location.street.name
        let postCode = currentData.location.postcode
        let city = currentData.location.city
        let picture = currentData.picture.large
        let user = new User(picture, firstName, lastName, age, email, streetNumber, streetName, postCode, city);

        USERS.push(user);
        console.log(USERS);
        usersContainer.insertAdjacentHTML('beforeend', renderUser(user))
        createIcons();
    }) .catch((error) => {
        console.log(error);
    })  
}










    //})
    //document.getElementById("newUser").innerHTML = `${USERS[0].firstName}`;
    //.catch(error) => {
        //console.log(error);
    //});





// fonction asynchrone, qui ne va pas attendre que promesse soit remplie pour afficher donc afficher vide 
//async function getUserData(){
    //Promise.all([
        //axios.get("https://randomuser.me/api/")
    //])
    //.then(response => {
       // const currentData = response.data.results[0];
       // const user = new User;
        //user.firstName = currentData.name.first
        //arrUser.push(user);
        //console.log(arrUser);
    //})




//     addNewUser.addEventListener('click', getUserData);

// //attend que la promesse soit ok pour afficher
// async function getUserData(){
//     //Promise.all([
//         //axios.get("https://randomuser.me/api/")
//     //])
//     //.then(response => {
//         const response = await axios.get("https://randomuser.me/api/")
//         const currentData = response.data.results[0];
//         //console.log(response)
        
//         const user = new User;
//         user.firstName = currentData.name.first
//         user.lastName = currentData.name.last
//         user.age = currentData.dob.age
//         user.email = currentData.email
//         user.streetNumber = currentData.location.street.number
//         user.streetName = currentData.location.street.name
//         user.postCode = currentData.location.postcode
//         user.city = currentData.location.city
//         user.picture = currentData.picture.medium
//         // console.log(user.firstName)
//         USERS.push(user);
//         console.log(USERS);
//         usersContainer.insertAdjacentHTML('beforeend', renderUser(USERS))
//     //})
//     //document.getElementById("newUser").innerHTML = `${USERS[0].firstName}`;
//     //.catch(error) => {
//         //console.log(error);
//     //});
// }
