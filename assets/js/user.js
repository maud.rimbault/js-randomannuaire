export class User{
    constructor(picture, firstName, lastName, age, email, streetNumber, streetName, postCode, city){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.postCode = postCode;
        this.city = city;
        this.picture = picture;

    }

    get fullname(){
        return `${this.firstName} ${this.lastName}`;
    }

    get address(){
        return `${this.streetNumber} ${this.streetName} ${this.postCode} ${this.city}`;
    }
}

export function renderUser(user) {
    const card =
        `<div class="image-container">
            <img src="${user.picture}"></img>
        </div>
        <div class="card-body">
            <h3> ${user.fullname} </h3>
            <div class="body-content">
                <p class="age"> <i icon-name="gift"></i> Age : ${user.age} </p>
                <p class="address"> <i icon-name="building"></i> ${user.address} </p>
                <p class="email"> <i icon-name="mail"></i> ${user.email} </p>
            </div>
        </div>`
    return `<div class="users_card"> ${card} </div>`
}

